package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class StudentTest {
  import Lists._
  import List._

  val t: String = "Viroli"
  val s: Student = Student("Rossi", 2019)
  val c: Course = Course("PPS", t)
  val c2: Course = Course("PCD", "Ricci")
  val c3: Course = Course("SDR", "D'Angelo")

  @Test def testCourse(): Unit ={
    assertEquals("PPS", c.name)
    assertEquals(t, c.teacher)
  }

  @Test def testSingleEnrolling(): Unit ={
    assertEquals(Nil(), s.courses)
    s.enrolling(c)
    assertEquals(Cons("PPS", Nil()), s.courses)
  }

  @Test def testHasTeacher(): Unit ={
    assertFalse(s.hasTeacher(t))
    s.enrolling(c)
    assertTrue(s.hasTeacher(t))
  }

  @Test def testVarArgEnrolling(): Unit ={
    assertEquals(Nil(), s.courses)
    assertFalse(s.hasTeacher(t))
    s.enrolling(c2, c, c3)
    assertEquals(Cons("SDR", Cons("PPS", Cons("PCD", Nil()))), s.courses)
    assertTrue(s.hasTeacher(t))
  }
}
