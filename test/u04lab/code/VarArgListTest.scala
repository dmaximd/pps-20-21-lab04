package u04lab.code

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test

class VarArgListTest {
  import Lists._
  import List._

  val emptyList: List[String] = List[String]()
  val l: List[String] = List[String]("a", "b", "c", "d")

  @Test
  def testVarArgList(): Unit ={
    assertEquals(Nil(), emptyList)
    assertEquals(Cons("a", Cons("b", Cons("c", Cons("d", Nil())))), l)
  }
}
