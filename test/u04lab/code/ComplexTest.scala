package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ComplexTest {
  import Complex._

  val z: Complex = Complex(0, 0)
  val c1: Complex = Complex(1, 5)
  val c2: Complex = Complex(1, 5)
  val c3: Complex = Complex(2, 3)

  @Test def testSum(): Unit ={
    assertEquals(c1, z + c1)
    assertEquals(Complex(2, 10), c1 + c2)
  }

  @Test def testProduct(): Unit ={
    assertEquals(z, z * c1)
    assertEquals(Complex(-24, 10), c1 * c2)
  }

  @Test def testEqualityAndPrint(): Unit ={
    println(c1)
    assertTrue(c1 == c2)
    assertFalse(c1 == c3)
  }

}
