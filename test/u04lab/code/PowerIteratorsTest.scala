package u04lab.code

import Optionals._
import Lists._
import Lists.List._
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions._

class PowerIteratorsTest {

  val factory = new PowerIteratorsFactoryImpl()

  @Test
  def testIncremental() {
    val pi = factory.incremental(5,_+2); // pi produce 5,7,9,11,13,...
    assertEquals(Option.of(5), pi.next());
    assertEquals(Option.of(7), pi.next());
    assertEquals(Option.of(9), pi.next());
    assertEquals(Option.of(11), pi.next());
    assertEquals(List.Cons(5, List.Cons(7, List.Cons(9, List.Cons(11,List.Nil())))), pi.allSoFar()); // elementi già prodotti
    for (i <- 0 until 10) {
      pi.next(); // procedo in avanti per un po'..
    }
    assertEquals(Option.of(33), pi.next()); // sono arrivato a 33
  }

  @Test
  def testRandom(): Unit ={
    val pi: PowerIterator[Boolean] = factory.randomBooleans(4)
    val b1 = Option.getOrElse(pi.next(), 0)
    val b2 = Option.getOrElse(pi.next(), 0)
    val b3 = Option.getOrElse(pi.next(), 0)
    val b4 = Option.getOrElse(pi.next(), 0)
    println(b1 + " " + b2 + " " + b3 + " " + b4)
    assertTrue(Option.isEmpty(pi.next()))
    assertEquals(pi.allSoFar(), Cons(b1, Cons(b2, Cons(b3, Cons(b4, Nil())))))
  }

  @Test
  def testFromList(): Unit ={
    val pi: PowerIterator[String] = factory.fromList(Cons("a", Cons("b", Cons("c", Nil()))))
    assertEquals(Option.Some("a"), pi.next())
    assertEquals(Option.Some("b"), pi.next())
    assertEquals(pi.allSoFar(), Cons("a", Cons("b", Nil())))
    assertEquals(Option.Some("c"), pi.next())
    assertEquals(pi.allSoFar(), Cons("a", Cons("b", Cons("c", Nil()))))
    assertTrue(Option.isEmpty(pi.next()))
  }

  @Test
  def testReversedOnList(): Unit ={
    val pi: PowerIterator[String] = factory.fromList(Cons("a", Cons("b", Cons("c", Nil()))))
    assertEquals(Option.Some("a"), pi.next())
    assertEquals(Option.Some("b"), pi.next())
    val pi2: PowerIterator[String] = pi.reversed()
    assertEquals(Option.Some("c"), pi.next())
    assertTrue(Option.isEmpty(pi.next()))

    assertEquals(Option.Some("b"), pi2.next())
    assertEquals(Option.Some("a"), pi2.next())
    assertEquals(pi2.allSoFar(), Cons("b", Cons("a", Nil())))
    assertTrue(Option.isEmpty(pi2.next()))
  }

  @Test
  def testReversedOnIncremental(): Unit ={
    val pi: PowerIterator[Int] = factory.incremental(0, _+1)
    assertEquals(Option.of(0), pi.next())
    assertEquals(Option.of(1), pi.next())
    assertEquals(Option.of(2), pi.next())
    assertEquals(Option.of(3), pi.next())
    val pi2: PowerIterator[Int] = pi.reversed()
    assertEquals(Option.of(3), pi2.next())
    assertEquals(Option.of(2), pi2.next())
    val pi3: PowerIterator[Int] = pi2.reversed()
    assertEquals(Option.of(2), pi3.next())
    assertEquals(Option.of(3), pi3.next())
    assertTrue(Option.isEmpty(pi3.next()))
  }
}