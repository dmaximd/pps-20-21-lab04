package u04lab.code

import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test

class ListTest {
  import Lists._
  import List._

  @Test def testContains(): Unit ={
    val l: List[Int] = Cons(2, Cons(5, Cons(7, Cons(11, Nil()))))

    assertTrue(contains(l)(7))
    assertFalse(contains(l)(4))
  }
}
