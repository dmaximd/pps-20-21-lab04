package u04lab.code

import Optionals._
import Lists._
import List._
import Streams._
import u04lab.code.Streams.Stream._

import scala.util.Random

trait PowerIterator[A] {
  def next(): Option[A]
  def allSoFar(): List[A]
  def reversed(): PowerIterator[A]
}

trait PowerIteratorsFactory {

  def incremental(start: Int, successive: Int => Int): PowerIterator[Int]
  def fromList[A](list: List[A]): PowerIterator[A]
  def randomBooleans(size: Int): PowerIterator[Boolean]
}

class PowerIteratorsFactoryImpl extends PowerIteratorsFactory {

  def fromStream[A](s: Stream[A]): PowerIterator[A] = {
    var stream: Stream[A] = s
    new PowerIterator[A] {
      var pastList: List[A] = Nil()

      override def next(): Option[A] = stream match {
        case Stream.Cons(head, tail) =>
          pastList = append(pastList, List.Cons(head(), Nil()))
          stream = tail()
          Option.Some(head())

        case Stream.Empty() => Option.empty
      }

      override def allSoFar(): List[A] = pastList

      override def reversed(): PowerIterator[A] = fromList(reverse(pastList))
    }
  }

  override def incremental(start: Int, successive: Int => Int): PowerIterator[Int] = fromStream(iterate(start)(successive))

  override def fromList[A](list: List[A]): PowerIterator[A] = fromStream(Stream.fromList(list))

  override def randomBooleans(size: Int): PowerIterator[Boolean] = fromStream(take(generate(Random.nextBoolean()))(size))
}
